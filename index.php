<!DOCTYPE html>
<html lang="fr">

<head>
    <title>Titre</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="bootstrap.min.css">
    <link rel="stylesheet" href="style.css">
</head>

<body>

    <?php
    try {
        $pdo = new PDO('mysql:host=localhost;dbname=exercices;charset=utf8', 'stagiaire13', 'stagiaire');
    } catch (Exception $e) {
        echo $e->message;
        die();
    }

    $stmt = $pdo->prepare('SELECT * FROM users WHERE picked = "true" '); // on selection tout les utilisateur qui on pas etait tirer au sort
    $stmt->execute();
    $res = $stmt->fetchAll(PDO::FETCH_ASSOC);


    ?>


    <main class="container">

        <div class="jumbotron">
            <h1 class="display-3">Tirage au sort !</h1>
            <p class="lead">Nous allons effectuer un tirage au sort !</p>

            <p class="lead">

                <button id="button_text" class="btn btn-primary btn-lg center" href="" onclick="refreshNumber()">Lancer le tirage !</button>
                <button id="button_text" class="btn btn-primary btn-lg center" href="" onclick="refreshDb()">Recommencez !</button>
                <button id="button_text" class="btn btn-primary btn-lg center" href="" onclick="showTirage()">Actualiser !</button>
                <p>Personne déjà tirer au sort : <br><br><span id="txtPersonne"><?php foreach ($res as $row) {
                                                                                    echo $row['prenom'] . PHP_EOL;
                                                                                } ?></span> </p>
            </p>
            <hr class="my-4">
            <p>Résultat : <span id="txtHint"></span> </p>
            </p>





        </div>


    </main>
    <script>
        function refreshNumber() {


            var xmlhttp = new XMLHttpRequest();
            xmlhttp.onreadystatechange = function() {
                if (this.readyState == 4 && this.status == 200) {
                    document.getElementById("txtHint").innerHTML = this.responseText;
                }
            };
            xmlhttp.open("GET", "request.php", true);
            xmlhttp.send();


        }



        function refreshDb() {


            var xmlhttp = new XMLHttpRequest();
            xmlhttp.onreadystatechange = function() {
                if (this.readyState == 4 && this.status == 200) {
                    document.getElementById("txtHint").innerHTML = this.responseText;
                }
            };
            xmlhttp.open("GET", "refresh.php", true);
            xmlhttp.send();
        }


        function showTirage() {

            var xmlhttp = new XMLHttpRequest();
            xmlhttp.onreadystatechange = function() {
                if (this.readyState == 4 && this.status == 200) {
                    document.getElementById("txtPersonne").innerHTML = this.responseText;
                }
            };
            xmlhttp.open("GET", "echo.php", true);
            xmlhttp.send();
        }

        
    </script>








    <script src="jquery.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</body>

</html>